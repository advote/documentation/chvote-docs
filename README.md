![CHVote logo](design/src/main/images/common/readme-header.jpg)

CHVote projects common documents
================================

- [General design documentation](design/README.md)

## Development directives
- [Git workflow](development-directives/git-workflow.md)
- [Java coding style](development-directives/coding-style-Java.md)
- [TypeScript coding style](development-directives/coding-style-FrontEnd.md)
- [Checklist for code review](development-directives/code-review-checklist.md)
- [Checklist for release review](development-directives/release-review-checklist.md)
- [Unit testing](development-directives/unit-testing-principles.md)
