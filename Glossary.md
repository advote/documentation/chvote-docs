# CHVote glossary
| Français| Anglais| Allemand|
| ------------- |:-------------:| -----:|
| Election      | Election| Wahl|
| Votation      | Vote      |   Abstimmung|
| Scrutin       |  Contest| Urnengang |
| Objet| Ballot| Vorlage|
| Election proportionnelle | Proportional election | Proporzwahl 
| Election majoritaire | Majority election | Majorzwahl
| Carte de vote | Voting card  | Stimmrechtsausweis
| Votation simple | Standard ballot  | Stimmrechtsausweis
| Votation complexe | Variant ballot | 
| Objet | Ballot |
| Question subsidiaire | Tie break question |
| Registre électoral  | Register | Stimm- und Wahlregister
| Electeur | Voter | Wählers
| Domaine d'influence  | Domain of influence| Wahlkreis
| Circonscription de dépouillement | Counting circle | Auszählkreis
| Canton | Canton | Kanton
| Commune | Municipality | Gemeinde
| Candidat | Candidate | Kandidat
| Local de vote | Place of voting | Stimmlokal
| Liste  | Liste  | Wahlliste
| Imprimeur  | Printer  | Drucker

