![CHVote logo](src/main/images/common/readme-header.jpg)
# CHVote-2.0 design documentation

## Architecture documents
- [System architecture](https://gitlab.com/dgsi-ve-private/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/architecture/system-architecture.pdf?job=artifact%3Adesign-docs)

## General system design documents
- [Protocol related keys management](https://gitlab.com/dgsi-ve-private/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/design/protocol-related-keys-management.pdf?job=artifact%3Adesign-docs)
- [Self test and integrity verifications](https://gitlab.com/dgsi-ve-private/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/design/self-test-integrity-verification-features.pdf?job=artifact%3Adesign-docs)

## Application design documents
* [Protocol core](https://gitlab.com/dgsi-ve-private/protocol-core/chvote-protocol/-/jobs/artifacts/master/raw/protocol-docs/target/generated-docs/pdf/protocol-application-design.pdf?job=build%3Adocs)
* [PACT](https://gitlab.com/dgsi-ve-private/tools/chvote-pact/builds/artifacts/master/raw/docs/target/generated-docs/pdf/pact-application-design.pdf?job=artifact%3Adocs)
* [Vote Receiver](https://gitlab.com/dgsi-ve-private/vote-receiver/chvote-receiver/builds/artifacts/master/raw/vote-receiver-design-docs/target/generated-docs/pdf/vote-receiver-application-design.pdf?job=artifact%3Adocs)
* [VRUM](https://gitlab.com/dgsi-ve-private/vrum/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-design-docs/target/generated-docs/pdf/vrum-application-design.pdf?job=artifacts%3Adesign-docs)
* [Backoffice](https://gitlab.com/dgsi-ve-private/bo/chvote-bo/builds/artifacts/master/raw/bo-design-docs/target/generated-docs/pdf/back-office-application-design.pdf?job=artifacts%3Adocs)

## Security documents
- [System threat model](https://gitlab.com/dgsi-ve-private/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/security/system-threat-model.pdf?job=artifact%3Adesign-docs)
- [Control components threat model](https://gitlab.com/dgsi-ve-private/protocol-core/chvote-protocol/-/jobs/artifacts/master/raw/docs/security/target/generated-docs/Threat%20modeling/control-components-threat-model.pdf?job=build%3Asecurity-docs)

## General development policies
- [Dependencies upgrade policy](https://gitlab.com/dgsi-ve-private/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/policy/dependencies-upgrade-policy.pdf?job=artifact%3Adesign-docs)

## Software factory design documents
- [Continuous integration and deployment](https://gitlab.com/dgsi-ve-private/chvote-docs/builds/artifacts/master/raw/design/target/generated-docs/pdf/development/continuous-integration-and-deployment.part.pdf?job=artifact%3Adesign-docs)

---

## Module documentation
- [Building the documentation](Build.md)
