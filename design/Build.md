![CHVote logo](src/main/images/common/readme-header.jpg)
# Documentation module build documentation

## Documentation source files structure
Documentation are to be placed under /src/main/ folder :
- `asciidoc/` contains asciidoc format documents
- `images/` contains images
- `images/generated` should stay empty. PlantUML diagrams images will be generated here
- `diagrams/` contains plantuml diagrams
- `sources/` contains application specific files used to generate the images (visio files for example).

## Documentation build process
- plantuml-maven-plugin is first used to render plantuml diagrams into PNG image files in src/main/images/generated
- asciidoctor-maven-plugin is then used to render asciidoc files (.adoc extension) into PDF files in target/generated-docs/

## How-tos and Tips

### How-to embed images in asciidoc files
Use the syntax image::\<path to image>[title]
Exemple : image::generated/architecture-model.png[Software architecture diagram]

### Use embedded GraphWiz renderer
PlantUML depends on GraphWiz to render compex diagrams. GraphWiz is platform-dependant and should be installed...
We provide a custom build image including GraphWiz since version 1.0.4 of `registry.gitlab.com/dgsi-ve-private/infra/docker-maven-oraclejdk8/docker-maven-oraclejdk8`

